//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport

class MyViewController : UIViewController {
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .black
        self.view = view
        
        let card = Card(sides: 3, point: CGPoint(x: 10, y: 10))
        card.backgroundColor = .white
        view.addSubview(card)
        
        let card2 = Card(sides: 4, point: CGPoint(x: 120, y: 10))
        card2.backgroundColor = .white
        view.addSubview(card2)
        
        let card3 = Card(sides: 5, point: CGPoint(x: 230, y: 10))
        card3.backgroundColor = .white
        view.addSubview(card3)
        
        let card4 = Card(sides: 6, point: CGPoint(x: 10, y: 180))
        card4.backgroundColor = .white
        view.addSubview(card4)
        
        let card5 = Card(sides: 7, point: CGPoint(x: 120, y: 180))
        card5.backgroundColor = .white
        view.addSubview(card5)
        
        let card6 = Card(sides: 8, point: CGPoint(x: 230, y: 180))
        card6.backgroundColor = .white
        view.addSubview(card6)
    }
}
class Card : UIView {
    var sides: Int?
    init(sides: Int, point: CGPoint) {
        self.sides = sides
        super.init(frame: CGRect(x: point.x, y: point.y, width: 100, height: 160))
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        if sides != nil && sides! > 0 {
            drawFigure(context: context, rect: rect, sides: sides!)
        }
        drawCustomText(context: context, rect: rect, text: String(sides!))
        context.setStrokeColor(UIColor.red.cgColor)
        context.setLineWidth(2)
        context.stroke(bounds)
    }
    
    func drawCustomText(context: CGContext, rect: CGRect, text: String){
        let tex: NSString = text as NSString
        let textRect = CGRect(x: rect.maxX/2 - 5, y: 20, width: 20, height: 20)
        
        let attributeDict: [NSAttributedString.Key : Any] = [
            .font: UIFont(name: "Helvetica Bold", size: 20.0),
        ]
        tex.draw(in: textRect, withAttributes: attributeDict)
    }
    
    func drawFigure(context: CGContext, rect: CGRect, sides: Int){
        // set margins
        let newMinX = rect.minX+5
        let newMinY = rect.minY+5
        let newMaxX = rect.maxX-5
        let newMaxY = rect.maxY-5
        
        let Radius = Double(newMaxX-newMinX)/2
        let SideLength = 2.0 * Radius * sin(Double.pi / Double(sides))
        var point = (x: Double(rect.maxX/2) + SideLength/2, y: Double(newMaxY))
        let angle = -360.0/Double(sides)
        // start from:
        context.move(to: CGPoint(x: point.x, y: point.y))
        // compute position
        for i in 1...sides{
            let sinFi = sin(Double(i) * angle * Double.pi / 180.0)
            let cosFi = cos(Double(i) * angle * Double.pi / 180.0)
            point.x = point.x + SideLength * cosFi
            point.y = point.y + SideLength * sinFi
            context.addLine(to: CGPoint(x: point.x, y: point.y))
        }
        context.closePath()
        context.setStrokeColor(UIColor.blue.cgColor)
        context.strokePath()
    }
}
// Present the view controller in the Live View window
PlaygroundPage.current.liveView = MyViewController()
